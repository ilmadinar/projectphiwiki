﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PHIWIKI.Models
{
    public class dashboard
    {
        public List<tbl_transaksi_buku> transaksi { get; set; }
        public List<View_orderbook> myorder { get; set; }
        public List<tbl_user> user { get; set; }
        public List<tbl_layanan_pengaduan> pengaduan { get; set; }
        public List<tbl_reimburse> reimburses { get; set; }
        public List<tbl_jawaban_quiz> quiz { get; set; }
        public List<tbl_timeline> timelines { get; set; }
    }
}