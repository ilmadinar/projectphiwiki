﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;


namespace PHIWIKI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Landing",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Landing", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Reimburse",
                url: "Reimburse/{Reimburse}",
                defaults: new { controller = "Reimburse", action = "Reimburse" }
            );

            routes.MapRoute(
                name: "Logout",
                url: "User/{Logout}",
                defaults: new { controller = "User", action = "Logout" }
            );

            routes.MapRoute(
                name: "Rating",
                url: "Rating/{Rating}",
                defaults: new { controller = "Rating", action = "Rating" }
            );

            routes.MapRoute(
               name: "Complaint",
               url: "Complaint/{Complaint}",
               defaults: new { controller = "Complaint", action = "Complaint" }
           );

            routes.MapRoute(
               name: "Review",
               url: "Review/{Review}",
               defaults: new { controller = "Review", action = "Review" }
           );

            routes.MapRoute(
               name: "Login",
               url: "User/{Login}",
               defaults: new { controller = "User", action = "Login" }
           );

            routes.MapRoute(
               name: "Registration",
               url: "User/{Registration}",
               defaults: new { controller = "User", action = "Registration" }
           );

            routes.MapRoute(
               name: "Jadwal",
               url: "Jadwal/{Jadwal}",
               defaults: new { controller = "Jadwal", action = "Jadwal" }
           );

            routes.MapRoute(
               name: "Index",
               url: "User/{Index}",
               defaults: new { controller = "User", action = "Index" }
           );

            routes.MapRoute(
                name: "Home",
                url: "Home/{Index}",
                defaults: new { controller = "Home", action = "Index" }
            );

            routes.MapRoute(
                name: "Tutorial",
                url: "Tutorial/{Tutorial}",
                defaults: new { controller = "Tutorial", action = "Tutorial" }
            );

            routes.MapRoute(
                name: "MasterRating",
                url: "MasterRating/{MasterRating}",
                defaults: new { controller = "MasterRating", action = "MasterRating" }
            );

            routes.MapRoute(
                name: "MasterReview",
                url: "MasterReview/{MasterReview}",
                defaults: new { controller = "MasterReview", action = "MasterReview" }
            );

            routes.MapRoute(
                name: "MasterComplaint",
                url: "MasterComplaint/{MasterComplaint}",
                defaults: new { controller = "MasterComplaint", action = "MasterComplaint" }
            );

            routes.MapRoute(
                name: "MasterRole",
                url: "MasterRole/{MasterRole}",
                defaults: new { controller = "MasterRole", action = "MasterRole" }
            );

            routes.MapRoute(
                name: "MasterBook",
                url: "MasterBook/{MasterBook}",
                defaults: new { controller = "MasterBook", action = "MasterBook" }
            );

            routes.MapRoute(
                name: "MasterQuiz",
                url: "MasterQuiz/{MasterQuiz}",
                defaults: new { controller = "MasterQuiz", action = "MasterQuiz" }
            );

            routes.MapRoute(
                name: "TimeLine",
                url: "TimeLine/{TimeLine}",
                defaults: new { controller = "TimeLine", action = "TimeLine" }
            );

            routes.MapRoute(
                name: "MasterCostumer",
                url: "MasterCostumer/{MasterCostumer}",
                defaults: new { controller = "MasterCostumer", action = "MasterCostumer" }
            );

            

            routes.MapRoute(
                name: "MyOrderList",
                url: "Order/{MyOrderList}",
                defaults: new { controller = "Order", action = "MyOrderList" }
            );

            routes.MapRoute(
                name: "MyOrder",
                url: "Order/{MyOrder}",
                defaults: new { controller = "Order", action = "MyOrder" }
            );

            routes.MapRoute(
               name: "MasterTutorial",
               url: "MasterTutorial/{MasterTutorial}",
               defaults: new { controller = "MasterTutorial", action = "MasterTutorial" }
           );

            routes.MapRoute(
               name: "Dashboard",
               url: "Dashboard/{Dashboard}",
               defaults: new { controller = "Dashboard", action = "Dashboard" }
           );

            routes.MapRoute(
               name: "OrderBook",
               url: "OrderBook/{OrderBook}",
               defaults: new { controller = "OrderBook", action = "OrderBook" }
           );

            routes.MapRoute(
               name: "MasterFunfact",
               url: "MasterFunfact/{MasterFunfact}",
               defaults: new { controller = "MasterFunfact", action = "MasterFunfact" }
           );

            routes.MapRoute(
               name: "Transaction",
               url: "Transaction/{Transaction}",
               defaults: new { controller = "Transaction", action = "Transaction" }
           );

            routes.MapRoute(
               name: "JawabanQuiz",
               url: "JawabanQuiz/{JawabanQuiz}",
               defaults: new { controller = "JawabanQuiz", action = "JawabanQuiz" }
           );

            routes.MapRoute(
               name: "Schedule",
               url: "Schedule/{Schedule}",
               defaults: new { controller = "Schedule", action = "Schedule" }
           );

            routes.MapRoute(
               name: "ChangePassword",
               url: "User/{ChangePassword}",
               defaults: new { controller = "User", action = "ChangePassword" }
           );

            routes.MapRoute(
               name: "ViewReimburse",
               url: "MasterReimburse/{ViewReimburse}",
               defaults: new { controller = "MasterReimburse", action = "ViewReimburse" }
           );

            routes.MapRoute(
               name: "ChangeProfile",
               url: "User/{ChangeProfile}",
               defaults: new { controller = "User", action = "ChangeProfile" }
           );

            routes.MapRoute(
              name: "ReportUser",
              url: "Report/{User}",
              defaults: new { controller = "Report", action = "User" }
          );
            routes.MapRoute(
              name: "ReportOrderBook",
              url: "Report/{OrderBook}",
              defaults: new { controller = "Report", action = "OrderBook" }
          );
            routes.MapRoute(
              name: "ReportQuiz",
              url: "Report/{Quiz}",
              defaults: new { controller = "Report", action = "Quiz" }
          );
            routes.MapRoute(
              name: "ReportComplaint",
              url: "Report/{Complaint}",
              defaults: new { controller = "Report", action = "Complaint" }
          );
            routes.MapRoute(
              name: "ReportReview",
              url: "Report/{Review}",
              defaults: new { controller = "Report", action = "Review" }
          );
            routes.MapRoute(
              name: "ReportReimburse",
              url: "Report/{Reimburse}",
              defaults: new { controller = "Report", action = "Reimburse" }
          );
            routes.MapRoute(
              name: "ReportTimeline",
              url: "Report/{Timeline}",
              defaults: new { controller = "Report", action = "Timeline" }
          );
            routes.MapRoute(
              name: "ReportRating",
              url: "Report/{Rating}",
              defaults: new { controller = "Report", action = "Rating" }
          );

            routes.MapRoute(
               name: "UserOrder",
               url: "Order/{UserOrder}",
               defaults: new { controller = "Order", action = "UserOrder" }
           );


            routes.MapRoute(
              name: "Permission",
              url: "User/{Permission}",
              defaults: new { controller = "User", action = "Permission" }
          );


        }


    }
}
