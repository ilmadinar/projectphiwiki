﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PHIWIKI.Models;

namespace phiwiki.Controllers
{
    public class ReimburseController : Controller
    {
        phiwikiEntities db = new phiwikiEntities();

        public ActionResult Reimburse()
        {
            ViewData["role"] = role();
            tbl_reimburse reimburse = new tbl_reimburse();
            return View(reimburse);
        }

        public List<tbl_role> role()
        {
            return db.tbl_role.ToList();

        }

        [HttpPost]
        public ActionResult Reimburse(string nama,int total_pengeluaran, string keterangan, string divisi, HttpPostedFileBase ImageFile)
        {
            tbl_reimburse r = new tbl_reimburse();

            if (ModelState.IsValid)
            {
                r.date = DateTime.Now;
                r.nama = nama;
                r.total_pengeluaran = total_pengeluaran;
                r.keterangan = keterangan;
                r.divisi = divisi;

                r.ImageFile = ImageFile;
                string fileName = Path.GetFileNameWithoutExtension(r.ImageFile.FileName);
                string extension = Path.GetExtension(r.ImageFile.FileName);
                fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                r.konfirmasi1 = "~/Image/reimburse/" + fileName;
                fileName = Path.Combine(Server.MapPath("~/Image/reimburse/"), fileName);
                r.ImageFile.SaveAs(fileName);


                r.fk_id_user = Convert.ToInt32(Session["id_user"]);
                r.konfirmasi2 = "not confirmed";
                db.tbl_reimburse.Add(r);
                db.SaveChanges();

                TempData["Reim"] = "Submit successful";
                TempData.Keep();
                return RedirectToAction("Reimburse");
            }

            ModelState.Clear();
            return View(r);


        }

        [HttpGet]
        public ActionResult View(int id)
        {
            tbl_reimburse image = new tbl_reimburse();
            image = db.tbl_reimburse.Where(x => x.id_reimburse == id).FirstOrDefault();
            return View(image);
        }
      
    }
}
