﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PHIWIKI.Models;

namespace phiwiki.Controllers
{
    public class LandingController : Controller
    {
        phiwikiEntities db = new phiwikiEntities();

        // GET: Landing
        public ActionResult Index()
        {
            ViewData["jenisbuku"] = jenisbuku();
            return View();
        }

        public List<tbl_jenisbuku> jenisbuku()
        {
            return db.tbl_jenisbuku.ToList();

        }

    }


}