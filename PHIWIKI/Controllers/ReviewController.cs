﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PHIWIKI.Models;

namespace phiwiki.Controllers
{
    public class ReviewController : Controller
    {
        phiwikiEntities db = new phiwikiEntities();

        public ActionResult Review()
        {
            ViewData["jenisbuku"] = jenisbuku();
            tbl_layanan_review review = new tbl_layanan_review();
            return View(review);
        }

        public List<tbl_jenisbuku> jenisbuku()
        {
            return db.tbl_jenisbuku.ToList();

        }

        [HttpPost]
        public ActionResult Review(string review, string jenisbuku)
        {
            if (Session["id_user"] == null)
            {
                return RedirectToAction("Index", "Landing");
            }
            else
            {
                int iduser = Convert.ToInt32(Session["id_user"]);
                var cek = db.view_myorder_transaksi.Where(x => x.fk_id_user == iduser && x.namabuku == jenisbuku && x.status == "done").FirstOrDefault();
                var cekReview = db.tbl_layanan_review.Where(x => x.fk_id_user == iduser && x.jenisbuku == jenisbuku).FirstOrDefault();

                tbl_layanan_review r = new tbl_layanan_review();

                if (cek != null)
                {
                    if(cekReview == null)
                    {
                        if (ModelState.IsValid)
                        {

                            r.date = DateTime.Now;
                            r.review = review;
                            r.jenisbuku = jenisbuku;

                            r.fk_id_user = Convert.ToInt32(Session["id_user"]);
                            db.tbl_layanan_review.Add(r);
                            db.SaveChanges();
                            TempData["feedback"] = "Your review has been sent!";
                            TempData.Keep();
                            return RedirectToAction("Rating", "Rating");
                        }
                    }
                    else
                    {
                        TempData["feedbackfail"] = "Cannot give review,you have already review this book";
                        TempData.Keep();
                        return RedirectToAction("Rating", "Rating");
                    }
                   

                }
                else
                {
                    TempData["feedbackfail"] = "Cannot give review, you haven't bought this book yet";
                    TempData.Keep();
                    return RedirectToAction("Rating", "Rating");
                }
                return View();
            }
            
        }

    }
}
