﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using PHIWIKI.Models;

namespace phiwiki.Controllers
{
    public class HomeController : Controller
    {
        phiwikiEntities db = new phiwikiEntities();
        public ActionResult Index(string username)
        {

            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id != "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                ViewData["jenisbuku"] = jenisbuku();
                ViewData["quiz"] = quiz();
                ViewData["funfact"] = funfact();
                
                return View(db.View_bookhome.ToList());
                
            }

        }

        public List<tbl_jenisbuku> jenisbuku()
        {
            return db.tbl_jenisbuku.ToList();

        }

        
        public List<tbl_quiz> quiz()
        {
            return db.tbl_quiz.ToList();

        }

        public List<tbl_funfact> funfact()
        {
            return db.tbl_funfact.ToList();

        }


        public ActionResult QuizAnswers()
        {
            tbl_jawaban_quiz quiz = new tbl_jawaban_quiz();
            return View(quiz);
        }

        [HttpPost]
        public ActionResult QuizAnswers(int id_quiz, string nama, string jawaban_quiz, string jawaban_benar2, string jawaban_benar3)
        {
            string email = (Session["email"]).ToString();
            int id = Convert.ToInt32(Session["id_user"]);
            var b = db.tbl_jawaban_quiz.Where(x => x.fk_id_user == id).FirstOrDefault();
            var quiz = db.tbl_quiz.Where(x => id_quiz == id_quiz).FirstOrDefault();
            tbl_jawaban_quiz jawab = new tbl_jawaban_quiz();
            int nilai1 = 0;
            int nilai2 = 0;
            int nilai3 = 0;

            //if (b == null | b.fk_id_quiz != id_quiz)
            if (b == null) 
            {
                if (ModelState.IsValid)
                {
                    jawab.fk_id_quiz = id_quiz;
                    jawab.fk_id_user = Convert.ToInt32(Session["id_user"]);
                    jawab.jawaban_quiz = jawaban_quiz;
                    jawab.jawaban_benar2 = jawaban_benar2;
                    jawab.jawaban_benar3 = jawaban_benar3;
                    jawab.nama = (Session["nama"]).ToString();


                    if (quiz.jawaban_benar == jawaban_quiz)
                    {
                        nilai1 = 33;
                    }
                    if (quiz.jawaban_benar2 == jawaban_benar2)
                    {
                        nilai2 = 33;
                    }
                    if (quiz.jawaban_benar3 == jawaban_benar3)
                    {
                        nilai3 = 34;
                    }

                    jawab.statusjawaban = (nilai1+nilai2+nilai3).ToString();
                    db.tbl_jawaban_quiz.Add(jawab);
                    db.SaveChanges();

                    SendMailScore(email, jawab.statusjawaban);
                    TempData["message"] = "Your answer has been sent! Cek your email for your score";
                    return RedirectToAction("Index");
                }
                return View(jawab);
            }else
            {
                if(b.fk_id_quiz != id_quiz)
                {
                    if (ModelState.IsValid)
                    {
                        jawab.fk_id_quiz = id_quiz;
                        jawab.fk_id_user = Convert.ToInt32(Session["id_user"]);
                        jawab.jawaban_quiz = jawaban_quiz;
                        jawab.jawaban_benar2 = jawaban_benar2;
                        jawab.jawaban_benar3 = jawaban_benar3;

                        if (quiz.jawaban_benar == jawaban_quiz)
                        {
                            nilai1 = 33;
                        }
                        if (quiz.jawaban_benar2 == jawaban_benar2)
                        {
                            nilai2 = 33;
                        }
                        if (quiz.jawaban_benar3 == jawaban_benar3)
                        {
                            nilai3 = 34;
                        }

                        jawab.statusjawaban = (nilai1 + nilai2 + nilai3).ToString();
                        db.tbl_jawaban_quiz.Add(jawab);
                        db.SaveChanges();
                        SendMailScore(email, jawab.statusjawaban);
                        TempData["message"] = "Your answer has been sent! Cek your email for your score";
                        return RedirectToAction("Index");
                       
                    }
                    return View(jawab);
                }
                else
                {
                    TempData["error"] = "sorry,you have joined this quiz session!";
                    return RedirectToAction("Index");
                }
 
            }
        }


        [HttpPost]
        public void SendMailScore(string email, string statusjawaban)
        {

            if (ModelState.IsValid)
            {
                var fromAddress = new MailAddress("order.phiwiki@gmail.com", "Phiwiki");
                var toAddress = new MailAddress(email);
                const string fromPassword = "phiwikinawadata";
                const string subject = "Phiwiki - Quiz";
                var body = "Hi, " +
                    "<br/>thank you for participated in the quiz session " +
                    "<br/>" +
                    "<br/>" +
                    "<br/>" +
                    "<br/>your score is  " + statusjawaban + " of 100" + 
                    "<br/>" +
                    "<br/> please wait for the annoucement" +
                    "<br/>" +
                    "<br/>if you are the winner, we will let you know by the mail!" +
                    "<br/>--" +
                    "<br/>This email is automatically sent from Phiwiki Web";

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)

                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                })
                {
                    smtp.Send(message);
                }
            }
        }



    }
}