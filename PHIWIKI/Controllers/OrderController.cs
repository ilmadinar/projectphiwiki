﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using PHIWIKI.Models;
using System.Transactions;

namespace phiwiki.Controllers
{
    public class OrderController : Controller
    {
        phiwikiEntities db = new phiwikiEntities();

        public ActionResult Order()
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id != "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                ViewData["jenisbuku"] = jenisbuku();
                tbl_transaksi_buku order = new tbl_transaksi_buku();
                return View(order);
            }
           
        }

        public List<tbl_jenisbuku> jenisbuku()
        {
            return db.tbl_jenisbuku.ToList();

        }

        public List<Jne> Jne()
        {
            return db.Jnes.ToList();

        }


        [HttpPost]
        public ActionResult Order(int id_user, string pilihanbuku, string jumlahbuku)
        {
            //var cekbuku = db.view_myorder_transaksi.Where(x => x.id_jenisbuku == pilihan_buku).FirstOrDefault();
            if (pilihanbuku != "" && jumlahbuku != "")
            {
                int pilihan_buku = Convert.ToInt32(pilihanbuku);
                int jumlah_buku= Convert.ToInt32(jumlahbuku);
                var buku = db.tbl_jenisbuku.Where(x => x.id_jenisbuku == pilihan_buku).FirstOrDefault();
                var cekID = db.view_myorder_transaksi.Where(x => x.fk_id_user == id_user && x.id_jenisbuku == pilihan_buku && x.status == "not yet order").FirstOrDefault();
                tbl_transaksi_buku r = new tbl_transaksi_buku();
                int id = Convert.ToInt32(Session["id_user"]);
                if (cekID != null)
                {
                    TempData["failed"] = "Failed to add the order! the type of this book is the same as the type of book you chose before. Please go to 'My Order' and add quantity";
                    TempData.Keep();
                    return RedirectToAction("Order");
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        r.fk_id_keranjangbayar = 0;
                        r.pilihan_buku = pilihan_buku.ToString();
                        r.jumlah_buku = jumlah_buku;
                        r.harga_satuan = Convert.ToInt32(buku.hargabuku);
                        r.harga_total = r.harga_satuan * r.jumlah_buku;
                        r.fk_id_user = Convert.ToInt32(Session["id_user"]);
                        r.status = "not yet order";

                        db.tbl_transaksi_buku.Add(r);

                        ViewData["total"] = r.harga_total;

                        db.SaveChanges();
                        TempData["order"] = "Successfully added to 'MyOrder'";
                        TempData.Keep();

                        return RedirectToAction("Order");
                    }

                    ModelState.Clear();
                    return View(r);
                }
            }
            else
            {
                TempData["failed"] = "All forms are required";
                return RedirectToAction("Order");
            }

        }


        public ActionResult MyOrderList()
        {
            int id_user = Convert.ToInt32(Session["id_user"].ToString());
            return View(db.view_myorder_transaksi.Where(x => x.fk_id_user == id_user && x.status=="not yet order").ToList());
        }

        public ActionResult Edit(tbl_transaksi_buku g)
        {
            var edit = db.tbl_transaksi_buku.Where(x => x.id_transaksi_buku == g.id_transaksi_buku).SingleOrDefault();
            try
            {
                edit.jumlah_buku = g.jumlah_buku;
                edit.harga_total = g.jumlah_buku * g.harga_satuan;
                db.SaveChanges();
                TempData["message"] = "Edit Success ! ";
            }
            catch (Exception Error)
            {
                throw Error;
            }
            return RedirectToAction("MyOrderList");
        }


        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    db.tbl_transaksi_buku.Remove(db.tbl_transaksi_buku.Find(id));
                    db.SaveChanges();
                    TempData["message"] = "Delete Success ! ";
                }
            }
            catch (Exception error)
            {
                throw error;
            }
            return RedirectToAction("MyOrderList");
        }


        public ActionResult MyOrder()
        {
            ViewData["deliv"] = Jne();
            int id_user = Convert.ToInt32(Session["id_user"].ToString());
            return View(db.tbl_transaksi_buku.Where(x => x.fk_id_user == id_user && x.status == "not yet order").ToList());
        }

        [HttpPost]
        public ActionResult MyOrder(string address, int idJne, HttpPostedFileBase ImageFile, view_count_totalbayar j)
        
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ImageFile != null)
                    {
                        string emailsession = Session["email"].ToString();
                        int id_user = Convert.ToInt32(Session["id_user"].ToString());
                        var b = db.Jnes.Where(x => x.Id == idJne).FirstOrDefault();
                        List<tbl_transaksi_buku> total = db.tbl_transaksi_buku.Where(x => x.fk_id_user == id_user && x.status == "not yet order").ToList();
                        tbl_keranjangbayar r = new tbl_keranjangbayar();
                        int total_hargabuku = 0;

                        if (ModelState.IsValid)
                        {
                            foreach (tbl_transaksi_buku item in total)
                            {
                                total_hargabuku = total_hargabuku + item.harga_total;
                            }
                            r.totalbayar = total_hargabuku + Convert.ToInt32(b.tarif_reg);
                            r.fk_Id_Jne = idJne;
                            r.address = address;
                            r.ImageFile = ImageFile;
                            string fileName = Path.GetFileNameWithoutExtension(r.ImageFile.FileName);
                            string extension = Path.GetExtension(r.ImageFile.FileName);
                            fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                            r.buktipembayaran = "~/Image/transaksi/" + fileName;
                            fileName = Path.Combine(Server.MapPath("~/Image/transaksi/"), fileName);
                            r.ImageFile.SaveAs(fileName);

                            ViewData["totalsemua"] = r.totalbayar;
                            r.fk_id_user = Convert.ToInt32(Session["id_user"]);
                            r.status = "not confirmed yet";
                            r.date = DateTime.Now;
                            if (idJne == 1 || idJne == 2)
                            {
                                r.deliv_status = "not yet taken";
                                //tandanya user akan ambil di itb
                            }
                            else
                            {
                                r.deliv_status = "not sent yet";
                                //barang nanti akan di deliv
                            }

                            db.tbl_keranjangbayar.Add(r);
                            ViewData["total"] = r.totalbayar;
                            db.SaveChanges();

                            foreach (tbl_transaksi_buku item in total)
                            {
                                item.fk_id_keranjangbayar = r.id_keranjangbayar;
                                item.status = "not confirmed yet";
                            }
                            db.SaveChanges();
                            transaction.Commit();
                            SendMailOrder(emailsession, r.id_keranjangbayar, r.totalbayar, r.fk_id_user, r.id_keranjangbayar);
                            TempData["order"] = "Your input is successful, cek your email for the details";
                            TempData.Keep();
                            return RedirectToAction("MyOrder");
                        }
                        //ModelState.Clear();
                        //return View(r);
                    }
                    else
                    {
                        TempData["failed"] = "All forms are required";
                        return RedirectToAction("MyOrder");
                    }
                }
                catch (Exception ex)
                {
                    TempData["failed"] = "fill out the form correctly";
                    transaction.Rollback();
                    return RedirectToAction("MyOrder");
                }
            }
            return View();
        }






        public JsonResult AjaxPostCallOngkir(Ongkir qtyData)
        {
            int id_user = Convert.ToInt32(Session["id_user"].ToString());
            int id_jne = Convert.ToInt16(qtyData.Qty);
            List<tbl_transaksi_buku> total = db.tbl_transaksi_buku.Where(x => x.fk_id_user == id_user && x.status == "not yet order").ToList();
            var b = db.Jnes.Where(x => x.Id == id_jne).FirstOrDefault();
            int total_hargabuku = 0;

            foreach (tbl_transaksi_buku item in total)
            {
                total_hargabuku = total_hargabuku + item.harga_total;
            }


            Ongkir idJne = new Ongkir
            {
                Qty = (Convert.ToInt32(b.tarif_reg) + Convert.ToInt32(total_hargabuku)).ToString()
            };
              return Json(idJne, JsonRequestBehavior.AllowGet);
        }

        public class Ongkir
        {
            public string Qty
            {
                get;
                set;
            }

        }



        [HttpPost]
        public void SendMailOrder(string email, int id, int totalbayar, int id_user, int idkeranjang)
        {
            List<View_orderbook> buku = db.View_orderbook.Where(x => x.fk_id_user == id_user && x.fk_id_keranjangbayar == idkeranjang).ToList();
            string listbuku = "";
            string List = "";
            int num = 1;
            foreach (View_orderbook item in buku)
            {
                listbuku = num + ". " +item.namabuku + " (" + item.jumlah_buku + " x Rp" + item.harga_satuan + "), " + "<br/>";
                List = List + listbuku;
                num++;
            }
            if (ModelState.IsValid)
            {

                var fromAddress = new MailAddress("order.phiwiki@gmail.com", "Phiwiki");
                var toAddress = new MailAddress(email);
                const string fromPassword = "phiwikinawadata";
                const string subject = "Phiwiki - Order";
                var body =
                    "<br/>You have made a payment transaction," +
                    "<br/>" +
                    "<br/>Here is your detail of order :" +
                    "<br/>" + List +
                    
                    "<br/>" +
                    "<br/>this is your transaction id >> " + id +
                    "<br/>" +
                    "<br/>this is the total to be paid >> Rp." + totalbayar +
                    "<br/>" +
                    "<br/>After the proof of payment has been accepted, we will send the confirmation of the payment to you." +
                    "<br/> For more information you can contact us with reply this email" +
                    "<br/>" +
                    "<br/>Thank you--" +
                    "<br/>This email is automatically sent from Phiwiki Web";

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)

                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                })
                {
                    smtp.Send(message);
                }
            }
        }



        public ActionResult UserOrder()
        {
            int id_user = Convert.ToInt32(Session["id_user"].ToString());
            return View(db.view_myorder_transaksi.Where(x => x.fk_id_user == id_user && x.status != "not yet order").ToList());
        }




    }
}
