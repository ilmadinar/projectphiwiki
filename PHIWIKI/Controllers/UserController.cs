﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using PHIWIKI.Models;
using PHIWIKI;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.Transactions;

namespace phiwiki.Controllers
{
    public class UserController : Controller
    {

        phiwikiEntities db = new phiwikiEntities();

        public ActionResult Registration()
        {
            tbl_user regis = new tbl_user();
            return View(regis);
        }

        [HttpPost]
        public ActionResult Registration(tbl_user r)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var User = db.tbl_user.Where(x => x.username.Equals(r.username)).FirstOrDefault();
                    var Email = db.tbl_user.Where(x => x.email.Equals(r.email)).FirstOrDefault();
                    Random rand = new Random();
                    string code = Membership.GeneratePassword(6, 0);
                    code = Regex.Replace(code, @"[^a-zA-Z0-9]", m => rand.Next(0, 10).ToString());

                    if (User != null)
                    {
                        TempData["erroruser"] = "user name already exists !";
                        TempData.Keep();
                        return RedirectToAction("Registration");
                    }
                    else
                    {
                        if (Email != null)
                        {
                            TempData["erroremail"] = "email already exists !";
                            TempData.Keep();
                            return RedirectToAction("Registration");
                        }
                        else
                        {
                            if (ModelState.IsValid)
                            {
                                //string Encrypt = Encryptor.EncrpytString(r.password_user.ToString());
                                r.password_user = Encryptor.EncrpytString(r.password_user.ToString());
                                r.confirmpassword = Encryptor.EncrpytString(r.confirmpassword.ToString());
                                r.status = "No";
                                r.fk_id_role = 1;
                                r.code = code;
                                db.tbl_user.Add(r);
                                db.SaveChanges();
                                transaction.Commit();
                                SendMailRegis(r.email, r.code);

                                TempData["success"] = "check your email to confirm your account !";
                                TempData.Keep();
                                return RedirectToAction("Registration");
                            }
                        }
                    }
                }
                 
                catch (Exception ex)
                {
                    TempData["erroremail"] = "fill out the form correctly";
                    transaction.Rollback();
                }
                return View(r);
            }
        }

        public ActionResult NewLogin(string code)
        {
            var user = db.tbl_user.Where(x => x.code == code).FirstOrDefault();
            if (user.status == "No")
            {
                user.confirmpassword = user.password_user;
                user.status = "Yes";
                db.SaveChanges();
                TempData["validYes"] = "Congratulations you have successfully confirmed your account";
                return RedirectToAction("Login", "User");
            }
            else
            {
                TempData["expreset"] = "sorry, the link for confirm account has expired";
                TempData.Keep();
                return RedirectToAction("Login", "User");
            }
        }





        public ActionResult Login()
        {
            if (Session["id_user"] != null)
            {
                return RedirectToAction("Index", "User", new { username = Session["username"].ToString() });
            }
            else
            {
                return View();
            }

        }

        [HttpPost]
        public ActionResult Login(tbl_user j)
        {
            try
            { 

                var b = db.tbl_user.Where(x => x.username.Equals(j.username)).FirstOrDefault();
                if (b != null)
                {
                    if (string.Compare(Encryptor.EncrpytString(j.password_user), b.password_user) == 0)
                    {
                        if (b.status == "Yes")
                        {
                            var role = db.tbl_role.Where(x => x.id_role == b.fk_id_role).FirstOrDefault();

                            Session["namarole"] = role.role_name;
                            Session["id_user"] = b.id_user;
                            Session["username"] = b.username;
                            Session["nama"] = b.nama_user;
                            Session["email"] = b.email;
                            Session["role"] = b.fk_id_role;
                            Session["code"] = b.code;
                            Session["nim"] = b.nim;
                            Session["fak"] = b.fakultas;
                            Session["kampus"] = b.kampus;
                            Session["no_hp"] = b.no_hp;
                            Session["pass"] = b.password_user;
                            Session["status"] = b.status;
                            string hashusername = Encryptor.EncrpytString(j.username);
                            return RedirectToAction("Index", "User", new { username = hashusername });
                        }
                        else
                        {
                            TempData["failed"] = "Your account has not been confirmed, please check your email!";
                            TempData.Keep();
                            return RedirectToAction("Login");
                        }
                    }
                        
                    else
                    {
                        TempData["failed"] = "Incorrect password !";
                        TempData.Keep();
                        return RedirectToAction("Login");

                    }

                }
                else
                {
                    TempData["failed"] = "Incorrect user name!";
                    TempData.Keep();
                    return RedirectToAction("Login");
                }
            }
            catch {
                return View("Login");
            }
            
        }

        

        public ActionResult Forgetpass()
        {
            tbl_forgotpassword forgot = new tbl_forgotpassword();
            return View(forgot);
        }

        [HttpPost]
        public ActionResult Forgetpass(tbl_forgotpassword forgot, string email)
        {

            Random rand = new Random();
            string code = Membership.GeneratePassword(6, 0);
            code = Regex.Replace(code, @"[^a-zA-Z0-9]", m => rand.Next(0, 10).ToString());
           
            var dataadmin = db.tbl_user.Where(x => x.email == email).FirstOrDefault();
            if (dataadmin != null)
            {
                try
                {
                    forgot.code = code;
                    forgot.name_user = dataadmin.email;
                    forgot.fk_id_user = dataadmin.id_user;
                    forgot.status = "Yes";
                    db.tbl_forgotpassword.Add(forgot);
                    db.SaveChanges();
                    SendMailForgot(dataadmin.email, forgot.id_forgotpassword, code);
                    TempData["cek"] = "Cek Your E-mail";
                    TempData.Keep();
                    return RedirectToAction("Forgetpass");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return RedirectToAction("Forgetpass");
                }
            }
            else
            {
                TempData["cek"] = "Email Wrong";
                TempData.Keep();
                return RedirectToAction("Forgetpass");
            }
            //return View();
        }
        
        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Login");
        }

        [HttpPost]
        public ActionResult Logout(tbl_user j)
        {

            Session.Abandon();
            return RedirectToAction("Login");

        }



        // GET: User
        public ActionResult Index(string username)
        {
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else
            {
                return View();
            }
           
        }

       
        
        public ActionResult Resetpass(string code, int forgot)
        {
            tbl_forgotpassword objforgot = db.tbl_forgotpassword.Where(x=>x.id_forgotpassword==forgot && x.status == "Yes" && x.code==code).FirstOrDefault();
            if (objforgot != null)
            {
                TempData["reset"] = objforgot.fk_id_user;
                TempData.Keep();
                return View();
            }
            else
            {
                TempData["expreset"] = "sorry, the link for reset password has expired";
                TempData.Keep();
                return RedirectToAction("Login", "User");
            }

        }

        [HttpPost]
        public ActionResult Resetpass(int id_user, string confirmpassword, string password)
        {
            if (ModelState.IsValid)
            {
                tbl_forgotpassword forgot = db.tbl_forgotpassword.Where(x => x.fk_id_user == id_user && x.status == "Yes").FirstOrDefault();
                tbl_user objUser = db.tbl_user.Where(x => x.id_user == id_user).FirstOrDefault();

                if (password == confirmpassword)
                {
                    objUser.confirmpassword = Encryptor.EncrpytString(password);
                    objUser.password_user = Encryptor.EncrpytString(password);
                    db.SaveChanges();
                    forgot.status = "No";
                    db.SaveChanges();
                    TempData["validYes"] = "Your password has been changed! Please, login";

                    return RedirectToAction("Login", "User");

                }
                else
                {
                    TempData["validNo"] = "Password Don't Match";
                }
            }
            return View();
        }

        


        [HttpPost]
        public void SendMailRegis(string email, string code)
        {
            
            if (ModelState.IsValid)
            {
                tbl_settings obj = db.tbl_settings.Where(x => x.setting_name == "base_url").FirstOrDefault();
                var fromAddress = new MailAddress("order.phiwiki@gmail.com", "Phiwiki");
                var toAddress = new MailAddress(email);
                const string fromPassword = "phiwikinawadata";
                const string subject = "Phiwiki - Registration";
                //string Encrypt = Encryptor.EncrpytString(user);
                
                var body = "Hi, "+
                    "<br/>Thanks for signing up to Phiwiki! " +
                    "<br/>" +
                    "<br/>" +
                    "<br/>To get started, click the link below to confirm your account" +
                    //"<br/> <a href='https://localhost:44319/User/Login'>Link</a>" +
                    //"<br/> <a href=" + "https://localhost:44319/User/NewLogin?code=" + code +"&id="+ id +">Link</a>" +
                    "<br/> <a href=" + obj.value_setting + "User/NewLogin?" + "code=" + code + ">Link</a>" +
                    "<br/>" +
                    "<br/>--" +
                    "<br/>This email is automatically sent from Phiwiki Web";

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)

                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                })
                {
                    smtp.Send(message);
                }
            }
        }




        [HttpPost]
        public void SendMailForgot(string email, int forgot, string code)
        {
            if (ModelState.IsValid)
            {

                tbl_settings obj= db.tbl_settings.Where(x => x.setting_name == "base_url").FirstOrDefault();
                var fromAddress = new MailAddress("order.phiwiki@gmail.com", "Phiwiki");
                var toAddress = new MailAddress(email);
                const string fromPassword = "phiwikinawadata";
                const string subject = "Phiwiki - Reset Password";
                
                var body = "Hi, " +
                    "<br/>Don't worry if you forgot password in Phiwiki! " +
                    "<br/>" +
                    "<br/>" +
                    "<br/>To get reset password, click the link below and change your password" +
                    //"<br/> <a href=" + "" + code + "&forgot=" + forgot + ">Link</a>" +
                    "<br/> <a href=" + obj.value_setting + "User/Resetpass?" + "code=" + code + "&forgot=" + forgot + ">link</a>" +
                    "<br/>" +
                    "<br/>--" +
                    "<br/>This email is automatically sent from Phiwiki Web";

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)

                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                })
                {
                    smtp.Send(message);
                }
            }
        }


        public ActionResult ChangePassword()
        {
            return View("ChangePassword");
        }

        [HttpPost]
        public ActionResult ChangePassword(int id_user, string confirmpassword, string password, string newpassword)
        {
            string passwordencrypt = Encryptor.EncrpytString(password);
            if (ModelState.IsValid)
            {
                tbl_user objUser = db.tbl_user.Where(x => x.id_user == id_user).FirstOrDefault();
                if (objUser.password_user == passwordencrypt)
                {
                    if (newpassword == confirmpassword)
                    {
                        objUser.confirmpassword = Encryptor.EncrpytString(newpassword);
                        objUser.password_user = Encryptor.EncrpytString(newpassword);
                        db.SaveChanges();
                        TempData["validYes"] = "Your password has been changed!";
                        //return RedirectToAction("Login");
                        return RedirectToAction("ChangePassword");

                    }
                    else
                    {
                        TempData["validNo"] = "Password Don't Match";
                        return RedirectToAction("ChangePassword");
                    }
                }
                else
                {
                    TempData["failedpass"] = "Your password is incorrect, please try again";
                    return RedirectToAction("ChangePassword");
                }
            }
            ModelState.Clear();
            return View();
        }




        public ActionResult ChangeProfile()
        {
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult ChangeProfile(tbl_user g)
        {
                var edit = db.tbl_user.Where(x => x.id_user == g.id_user).SingleOrDefault();
                try
                {
                    edit.id_user = g.id_user;
                    edit.kampus = g.kampus;
                    edit.nama_user = g.nama_user;
                    edit.nim = g.nim;
                    edit.no_hp = g.no_hp;
                    edit.password_user = Session["pass"].ToString();
                    edit.status = Session["status"].ToString();
                    edit.fakultas = g.fakultas;
                    edit.confirmpassword = Session["pass"].ToString();
                    edit.fk_id_role = Convert.ToInt32(Session["role"]);
                    edit.code = Session["code"].ToString();
                    edit.email = Session["email"].ToString();
                    edit.username = Session["username"].ToString();
                    db.SaveChanges();

                Session["nama"] = edit.nama_user;
                Session["nim"] = edit.nim;
                Session["fak"] = edit.fakultas;
                Session["kampus"] = edit.kampus;
                Session["no_hp"] = edit.no_hp;
                db.SaveChanges();

                TempData["editprofile"] = "Edit Profile Success ! ";
                }
                catch (Exception Error)
                {
                    throw Error;
                }
                return RedirectToAction("ChangeProfile");
        }


        public ActionResult Permission()
        {
            return View();

        }


    }
}
