﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PHIWIKI.Models;

namespace phiwiki.Controllers
{
    public class ComplaintController : Controller
    {
        phiwikiEntities db = new phiwikiEntities();

        public ActionResult Complaint()
        {
            ViewData["jenisbuku"] = jenisbuku();
            tbl_layanan_pengaduan comp = new tbl_layanan_pengaduan();
            return View(comp);
        }
        public List<tbl_jenisbuku> jenisbuku()
        {
            return db.tbl_jenisbuku.ToList();

        }

        [HttpPost]
        public ActionResult Complaint(string pengaduan, string jenisbuku)
        {

            int iduser = Convert.ToInt32(Session["id_user"]);
            var cek = db.view_myorder_transaksi.Where(x => x.fk_id_user == iduser && x.namabuku == jenisbuku && x.status == "done").FirstOrDefault();

            tbl_layanan_pengaduan r = new tbl_layanan_pengaduan();

            if (cek != null)
            {
            if (ModelState.IsValid)
            {
                r.date = DateTime.Now;
                r.pengaduan = pengaduan;
                r.jenisbuku = jenisbuku;
                r.fk_id_user = Convert.ToInt32(Session["id_user"]);
                db.tbl_layanan_pengaduan.Add(r);
                db.SaveChanges();
                TempData["feedback"] = "Your complaint has been sent";
                TempData.Keep();
                return RedirectToAction("Rating", "Rating");
            }

            return View(r);
            }
            else
            {
                TempData["feedbackfail"] = "Cannot give complaint, you haven't bought this book yet";
                TempData.Keep();
                return RedirectToAction("Rating", "Rating");
            }

        }


    }
}
