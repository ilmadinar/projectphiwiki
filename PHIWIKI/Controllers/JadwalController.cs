﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PHIWIKI.Models;

namespace phiwiki.Controllers
{
    public class JadwalController : Controller
    {
        // GET: Jadwal
        phiwikiEntities db = new phiwikiEntities();

        public ActionResult Jadwal()
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id != "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                return View(db.tbl_jadwal.ToList());
            }
            //return View(db.tbl_jadwal.ToList());
        }



        public ActionResult Index()
        {
            return View();
        }

        // GET: Jadwal/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Jadwal/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Jadwal/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Jadwal/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Jadwal/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Jadwal/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Jadwal/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
