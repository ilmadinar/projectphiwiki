﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PHIWIKI.Models;

namespace phiwiki.Controllers.Controller_Admin
{
    public class MasterQuizController : Controller
    {
        phiwikiEntities db = new phiwikiEntities();
        // GET: MasterRole
        public ActionResult MasterQuiz()
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id == "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                return View(db.tbl_quiz.ToList());
            }
        }

        public ActionResult Create(HttpPostedFileBase ImageFile, string soal_quiz, string jawaban_benar, string jawaban_benar2, string jawaban_benar3)
        {
            tbl_quiz r = new tbl_quiz();
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    r.ImageFile = ImageFile;
                    string fileName = Path.GetFileNameWithoutExtension(r.ImageFile.FileName);
                    string extension = Path.GetExtension(r.ImageFile.FileName);
                    fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                    r.gambar = "~/Image/soal_quiz/" + fileName;
                    fileName = Path.Combine(Server.MapPath("~/Image/soal_quiz/"), fileName);
                    r.ImageFile.SaveAs(fileName);

                    r.jawaban_benar = jawaban_benar;
                    r.jawaban_benar2 = jawaban_benar2;
                    r.jawaban_benar3 = jawaban_benar3;
                    r.soal_quiz = soal_quiz;

                    db.tbl_quiz.Add(r);
                    db.SaveChanges();
                    TempData["message"] = "Create Success ! ";
                }
                catch (Exception error)
                {
                    TempData["error"] = "All forms are required";
                    transaction.Rollback();
                }

                return RedirectToAction("MasterQuiz");
            }
        }

        public ActionResult Edit(int id_quiz, string soal_quiz, string jawaban_benar, string jawaban_benar2, string jawaban_benar3, HttpPostedFileBase ImageFile)
        {
            var edit = db.tbl_quiz.Where(x => x.id_quiz == id_quiz).SingleOrDefault();
            try
            {
                String fullPath = Request.MapPath(edit.gambar);
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }

                edit.ImageFile = ImageFile;
                string fileName = Path.GetFileNameWithoutExtension(edit.ImageFile.FileName);
                string extension = Path.GetExtension(edit.ImageFile.FileName);
                fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                edit.gambar = "~/Image/soal_quiz/" + fileName;
                fileName = Path.Combine(Server.MapPath("~/Image/soal_quiz/"), fileName);
                edit.ImageFile.SaveAs(fileName);

                edit.soal_quiz = soal_quiz;
                edit.jawaban_benar = jawaban_benar;
                edit.jawaban_benar2 = jawaban_benar2;
                edit.jawaban_benar3 = jawaban_benar3;
                db.SaveChanges();
                TempData["message"] = "Edit Success ! ";
            }
            catch (Exception Error)
            {
                throw Error;
            }
            return RedirectToAction("MasterQuiz");
        }

        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    var edit = db.tbl_quiz.Where(x => x.id_quiz == id).SingleOrDefault();
                    String fullPath = Request.MapPath(edit.gambar);
                    if (System.IO.File.Exists(fullPath))
                    {
                        System.IO.File.Delete(fullPath);
                    }

                    db.tbl_quiz.Remove(db.tbl_quiz.Find(id));
                    db.SaveChanges();
                    TempData["message"] = "Delete Success ! ";
                }
            }
            catch (Exception error)
            {
                throw error;
            }
            return RedirectToAction("MasterQuiz");
        }
    }
}