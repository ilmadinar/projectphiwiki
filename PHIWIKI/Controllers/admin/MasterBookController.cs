﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PHIWIKI.Models;

namespace phiwiki.Controllers.Controller_Admin
{
    public class MasterBookController : Controller
    {
        phiwikiEntities db = new phiwikiEntities();
        // GET: OrderBook
        public ActionResult MasterBook()
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id == "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                return View(db.tbl_jenisbuku.ToList());
            }
        }       

        [HttpPost]
        public ActionResult Create(HttpPostedFileBase ImageFile, int hargabuku, string namabuku, string penulis, string tahun_terbit, string deskripsi, string rating)
        {
            tbl_jenisbuku r = new tbl_jenisbuku();
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {

                        r.hargabuku = hargabuku.ToString();
                        r.namabuku = namabuku;
                        r.penulis = penulis;
                        r.tahun_terbit = tahun_terbit;
                        r.deskripsi = deskripsi;
                        r.rating = rating;
                        r.ImageFile = ImageFile;
                        string fileName = Path.GetFileNameWithoutExtension(r.ImageFile.FileName);
                        string extension = Path.GetExtension(r.ImageFile.FileName);
                        fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                        r.gambarbuku = "~/Image/jenisbuku/" + fileName;
                        fileName = Path.Combine(Server.MapPath("~/Image/jenisbuku/"), fileName);
                        r.ImageFile.SaveAs(fileName);

                        db.tbl_jenisbuku.Add(r);
                        db.SaveChanges();
                      
                        TempData["message"] = "Submit successful";
                        TempData.Keep();
                        return RedirectToAction("MasterBook");
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    TempData["error"] = "All forms are required";
                    transaction.Rollback();
                    return RedirectToAction("MasterBook");
                }

                //ModelState.Clear();
                return View(r);
            } 
        }




        public ActionResult Edit(tbl_jenisbuku g, HttpPostedFileBase ImageFile)
        {
            var edit = db.tbl_jenisbuku.Where(x => x.id_jenisbuku == g.id_jenisbuku).SingleOrDefault();
            string idbuku = (g.id_jenisbuku).ToString();
            var MyOrder = db.tbl_transaksi_buku.Where(x => x.pilihan_buku == idbuku && x.status == "not yet order").ToList();
            try
            {
                String fullPath = Request.MapPath(edit.gambarbuku);
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }

                edit.ImageFile = ImageFile;
                string fileName = Path.GetFileNameWithoutExtension(edit.ImageFile.FileName);
                string extension = Path.GetExtension(edit.ImageFile.FileName);
                fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                edit.gambarbuku = "~/Image/jenisbuku/" + fileName;
                fileName = Path.Combine(Server.MapPath("~/Image/jenisbuku/"), fileName);
                edit.ImageFile.SaveAs(fileName);


                edit.namabuku = g.namabuku;
                edit.hargabuku = g.hargabuku;
                edit.penulis = g.penulis;
                edit.tahun_terbit = g.tahun_terbit;
                edit.rating = g.rating;
                edit.deskripsi = g.deskripsi;
                db.SaveChanges();

                foreach (tbl_transaksi_buku item in MyOrder)
                {
                    item.harga_satuan = Convert.ToInt32(g.hargabuku);
                    item.harga_total = item.jumlah_buku * Convert.ToInt32(g.hargabuku);
                }

                db.SaveChanges();
                TempData["message"] = "Edit Success ! ";
            }
            catch (Exception Error)
            {
                throw Error;
            }
            return RedirectToAction("MasterBook");
        }

        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    var edit = db.tbl_jenisbuku.Where(x => x.id_jenisbuku == id).SingleOrDefault();
                    string fullPath = Request.MapPath(edit.gambarbuku);
                    if (System.IO.File.Exists(fullPath))
                    {
                        System.IO.File.Delete(fullPath);
                    }

                    db.tbl_jenisbuku.Remove(db.tbl_jenisbuku.Find(id));
                    db.SaveChanges();
                    TempData["message"] = "Delete Success ! ";
                }
            }
            catch (Exception error)
            {
                throw error;
            }
            return RedirectToAction("MasterBook");
        }
    }
}