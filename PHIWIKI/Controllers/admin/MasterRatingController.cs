﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PHIWIKI.Models;

namespace phiwiki.Controllers.Controller_Admin
{
    public class MasterRatingController : Controller
    {
        phiwikiEntities db = new phiwikiEntities();
        // GET: MasterRating
        public ActionResult MasterRating()
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id == "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                return View(db.View_rating.ToList());
            }
        }
        
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    db.tbl_layanan_rating.Remove(db.tbl_layanan_rating.Find(id));
                    db.SaveChanges();
                    TempData["message"] = "Delete Success ! ";
                }
            }
            catch (Exception error)
            {
                throw error;
            }
            return RedirectToAction("MasterRating");
        }
    }
}