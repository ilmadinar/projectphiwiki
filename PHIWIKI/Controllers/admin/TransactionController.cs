﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using PHIWIKI.Models;

namespace phiwiki.Controllers.Controller_Admin
{
    public class TransactionController : Controller
    {
        phiwikiEntities db = new phiwikiEntities();
        // GET: Transaction

        public ActionResult Transaction()
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id == "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                return View(db.View_keranjangbayar.ToList());
            }
        }

        public ActionResult Edit(tbl_keranjangbayar g, int fk_id_user)
        {
            var edit = db.tbl_keranjangbayar.Where(x => x.id_keranjangbayar == g.id_keranjangbayar).FirstOrDefault();
            var user = db.tbl_user.Where(x => x.id_user == fk_id_user).FirstOrDefault();
            List<tbl_transaksi_buku> status = db.tbl_transaksi_buku.Where(x => x.fk_id_user == fk_id_user && x.fk_id_keranjangbayar == g.id_keranjangbayar).ToList();
            try
            {
                
                edit.deliv_status = g.deliv_status;
                db.SaveChanges();


                if (edit.deliv_status == "has been sent" || edit.deliv_status == "ITB" && edit.status == "accepted")
                {
                    foreach (tbl_transaksi_buku item in status)
                    {
                        item.status = "done";
                    }
                }

                db.SaveChanges();
                TempData["message"] = "Edit Success ! ";
            }
            catch (Exception Error)
            {
                throw Error;
            }
            return RedirectToAction("Transaction");
        }


        public ActionResult EditStatus(tbl_keranjangbayar g, int fk_id_user)
        {
            var edit = db.tbl_keranjangbayar.Where(x => x.id_keranjangbayar == g.id_keranjangbayar).FirstOrDefault();
            var user = db.tbl_user.Where(x => x.id_user == fk_id_user).FirstOrDefault();
            List<tbl_transaksi_buku> status = db.tbl_transaksi_buku.Where(x => x.fk_id_user == fk_id_user && x.fk_id_keranjangbayar == g.id_keranjangbayar).ToList();
            try
            {
                edit.status = g.status;
               
                db.SaveChanges();

                if (edit.status == "accepted")
                {
                    SendMailTransaction(user.email, edit.totalbayar,edit.fk_id_user,edit.id_keranjangbayar);
                    foreach (tbl_transaksi_buku item in status)
                    {
                        item.status = "pending";
                    }
                }
                else if (edit.status == "rejected")
                {

                    SendMailTransactionfail(user.email, edit.totalbayar, edit.fk_id_user, edit.id_keranjangbayar);
                    foreach (tbl_transaksi_buku item in status)
                    {
                        item.status = "rejected";
                    }
                }
                db.SaveChanges();
                TempData["message"] = "Edit Success ! ";
            }
            catch (Exception Error)
            {
                throw Error;
            }
            return RedirectToAction("Transaction");
        }


        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    db.tbl_keranjangbayar.Remove(db.tbl_keranjangbayar.Find(id));
                    db.SaveChanges();
                    TempData["message"] = "Delete Success ! ";
                }
            }
            catch (Exception error)
            {
                throw error;
            }
            return RedirectToAction("Transaction");
        }




        [HttpPost]
        public void SendMailTransaction(string email, int totalbayar, int id_user, int idkeranjang)
        {
            List<View_orderbook> buku = db.View_orderbook.Where(x => x.fk_id_user == id_user && x.fk_id_keranjangbayar == idkeranjang).ToList();
            string listbuku = "";
            string List = "";
            int num = 1;
            foreach (View_orderbook item in buku)
            {
                listbuku = num + ". " + item.namabuku + " (" + item.jumlah_buku + " x Rp" + item.harga_satuan + "), " + "<br/>";
                List = List + listbuku;
                num++;
            }

            if (ModelState.IsValid)
            {

                var fromAddress = new MailAddress("order.phiwiki@gmail.com", "Phiwiki");
                var toAddress = new MailAddress(email);
                const string fromPassword = "phiwikinawadata";
                const string subject = "Phiwiki - Order Accepted";
                var body =
                    "<br/>Your payment has been confirmed," +
                    "<br/>" +
                    "<br/>" +
                    "<br/>you have paid >> Rp." + totalbayar +
                    "<br/>" +
                     "<br/>Here is your detail of order :" +
                    "<br/>" + List +
                    "<br/>Please wait for the shipping information." +
                    "<br/> For more information you can contact us by replying to this email" +
                    "<br/>" +
                    "<br/>Thank you--" +
                    "<br/>This email is automatically sent from Phiwiki Web";

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)

                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                })
                {
                    smtp.Send(message);
                }
            }
        }



        [HttpPost]
        public void SendMailTransactionfail(string email, int totalbayar, int id_user, int idkeranjang)
        {
            List<View_orderbook> buku = db.View_orderbook.Where(x => x.fk_id_user == id_user && x.fk_id_keranjangbayar == idkeranjang).ToList();
            string listbuku = "";
            string List = "";
            int num = 1;
            foreach (View_orderbook item in buku)
            {
                listbuku = num + ". " + item.namabuku + " (" + item.jumlah_buku + " x Rp" + item.harga_satuan + "), " + "<br/>";
                List = List + listbuku;
                num++;
            }

            if (ModelState.IsValid)
            {

                var fromAddress = new MailAddress("order.phiwiki@gmail.com", "Phiwiki");
                var toAddress = new MailAddress(email);
                const string fromPassword = "phiwikinawadata";
                const string subject = "Phiwiki - Order Rejected";
                var body =
                    "<br/>Your payment is rejected," +
                    "<br/>" +
                    "<br/>" +
                    "<br/>you have paid >> Rp." + totalbayar +
                    "<br/>" +
                     "<br/>Here is your detail of order :" +
                    "<br/>" + List +
                    "<br/>we will return your money immediately" +
                    "<br/> For more information you can contact us by replying to this email" +
                    "<br/>" +
                    "<br/>Thank you--" +
                    "<br/>This email is automatically sent from Phiwiki Web";

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)

                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                })
                {
                    smtp.Send(message);
                }
            }
        }


    }
}