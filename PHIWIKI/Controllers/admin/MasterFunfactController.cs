﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PHIWIKI.Models;

namespace phiwiki.Controllers.Controller_Admin
{
    public class MasterFunfactController : Controller
    {
        phiwikiEntities db = new phiwikiEntities();
        // GET: MasterFunfact
        public ActionResult MasterFunfact()
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id == "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                return View(db.tbl_funfact.ToList());
            }
        }


        [HttpPost]
        public ActionResult Create(HttpPostedFileBase ImageFile, string nama_funfact)
        {
            tbl_funfact r = new tbl_funfact();
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        r.nama_funfact = nama_funfact;
                        r.ImageFile = ImageFile;
                        string fileName = Path.GetFileNameWithoutExtension(r.ImageFile.FileName);
                        string extension = Path.GetExtension(r.ImageFile.FileName);
                        fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                        r.funfact = "~/Image/funfact/" + fileName;
                        fileName = Path.Combine(Server.MapPath("~/Image/funfact/"), fileName);
                        r.ImageFile.SaveAs(fileName);

                        db.tbl_funfact.Add(r);
                        db.SaveChanges();
                        TempData["tutorial"] = "Submit successful";
                        TempData.Keep();
                        return RedirectToAction("MasterFunfact");
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    TempData["error"] = "All forms are required";
                    transaction.Rollback();
                    return RedirectToAction("MasterFunfact");
                }
                return RedirectToAction("MasterFunfact");
            }

        }




        public ActionResult Edit(tbl_funfact g)
        {
            var edit = db.tbl_funfact.Where(x => x.id_funfact == g.id_funfact).SingleOrDefault();
            try
            {
                String fullPath = Request.MapPath(edit.funfact);
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }

                string fileName = Path.GetFileNameWithoutExtension(edit.ImageFile.FileName);
                string extension = Path.GetExtension(edit.ImageFile.FileName);
                fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                edit.funfact = "~/Image/jenisbuku/" + fileName;
                fileName = Path.Combine(Server.MapPath("~/Image/jenisbuku/"), fileName);
                edit.ImageFile.SaveAs(fileName);


                edit.nama_funfact = g.nama_funfact;

                db.SaveChanges();
                TempData["message"] = "Edit Success ! ";
            }
            catch (Exception Error)
            {
                throw Error;
            }
            return RedirectToAction("MasterFunfact");
        }

        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    db.tbl_funfact.Remove(db.tbl_funfact.Find(id));
                    db.SaveChanges();
                    TempData["message"] = "Delete Success ! ";
                }
            }
            catch (Exception error)
            {
                throw error;
            }
            return RedirectToAction("MasterFunfact");
        }


    }
}