﻿using Microsoft.Reporting.WebForms;
using PHIWIKI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace PHIWIKI.Controllers.Controller_Admin
{
    public class ReportController : Controller
    {
        phiwikiEntities ds = new phiwikiEntities();
        ReportViewer report = new ReportViewer();

        public ActionResult User(string filter)
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id == "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {

                report.ProcessingMode = ProcessingMode.Local;
                report.Width = Unit.Pixel(910);
                string queries = "";
                switch (filter)
                {
                    case "1":
                        queries = "select * from View_role_customer where id_role =" + 1;
                        break;
                    case "2":
                        queries = "select * from View_role_customer where id_role =" + 2;
                        break;
                    case "3":
                        queries = "select * from View_role_customer where id_role =" + 3;
                        break;
                    case "4":
                        queries = "select * from View_role_customer where id_role =" + 4;
                        break;
                    case "5":
                        queries = "select * from View_role_customer where id_role =" + 5;
                        break;
                    case "6":
                        queries = "select * from View_role_customer where id_role =" + 6;
                        break;
                    default:
                        queries = "select * from View_role_customer";
                        break;
                }
                GetDataUser(queries);
                var dataactor = ds.tbl_role.ToList();
                return View(dataactor);
            }
        }



        public void GetDataUser(string queries)
        {

            UserDataSet ds = new UserDataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["phiwikiConnectionString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(queries, conn);
            dataAdapter.Fill(ds, ds.View_role_customer.TableName);
            report.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Report\User.rdlc";
            report.LocalReport.DataSources.Clear();
            report.LocalReport.DataSources.Add(new ReportDataSource("User", ds.Tables[0]));
            report.LocalReport.Refresh();
            ViewBag.ReportViewer = report;
        }

        public ActionResult Reimburse(string filter)
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id == "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                report.ProcessingMode = ProcessingMode.Local;
                report.Width = Unit.Pixel(910);
                string queries = "";
                switch (filter)
                {
                    case "1":
                        queries = "select * from tbl_reimburse where konfirmasi2 ='not confirmed'";
                        break;
                    case "2":
                        queries = "select * from tbl_reimburse where konfirmasi2 ='accepted'";
                        break;
                    case "3":
                        queries = "select * from tbl_reimburse where konfirmasi2 ='rejected'";
                        break;
                    case "4":
                        queries = "select * from tbl_reimburse where divisi ='superadmin'";
                        break;
                    case "5":
                        queries = "select * from tbl_reimburse where divisi ='logistics'";
                        break;
                    case "6":
                        queries = "select * from tbl_reimburse where divisi ='treasurer'";
                        break;
                    case "7":
                        queries = "select * from tbl_reimburse where divisi ='problem solver'";
                        break;
                    default:
                        queries = "select * from tbl_reimburse";
                        break;
                }
                GetDataReimburse(queries);
                var dataactor = ds.tbl_reimburse.ToList();
                return View(dataactor);
            }
        }
        public void GetDataReimburse(string queries)
        {
            ReimburseDataSet ds = new ReimburseDataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["phiwikiConnectionString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(queries, conn);
            dataAdapter.Fill(ds, ds.tbl_reimburse.TableName);
            report.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Report\Reimburse.rdlc";
            report.LocalReport.DataSources.Add(new ReportDataSource("DataSetReimburse", ds.Tables[0]));
            ViewBag.ReportViewer = report;
        }

        public ActionResult OrderBook(string filter)
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id == "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                report.ProcessingMode = ProcessingMode.Local;
                report.Width = Unit.Pixel(910);
                string queries = "";
                switch (filter)
                {
                    case "1":
                        queries = "select * from View_orderbook where status ='not confirmed yet'";
                        break;
                    case "2":
                        queries = "select * from View_orderbook where status ='accepted'";
                        break;
                    case "3":
                        queries = "select * from View_orderbook where status ='not yet order'";
                        break;
                    case "4":
                        queries = "select * from View_orderbook where namabuku ='phiwiki 2019'";
                        break;
                    case "5":
                        queries = "select * from View_orderbook where namabuku ='phiwiki 2020'";
                        break;
                    case "6":
                        queries = "select * from View_orderbook where namabuku ='phiwiki II 2020'";
                        break;
                    default:
                        queries = "select * from View_orderbook";
                        break;
                }
                GetDataOrderBook(queries);
                var dataactor = ds.View_orderbook.ToList();
                return View(dataactor);
            }
        }
        public void GetDataOrderBook(string queries)
        {
            OrderDataSet ds = new OrderDataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["phiwikiConnectionString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(queries, conn);
            dataAdapter.Fill(ds, ds.View_orderbook.TableName);
            report.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Report\OrderBook.rdlc";
            report.LocalReport.DataSources.Add(new ReportDataSource("OrderBook", ds.Tables[0]));
            ViewBag.ReportViewer = report;
        }

        public ActionResult Complaint(string filter)
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id == "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                report.ProcessingMode = ProcessingMode.Local;
                report.Width = Unit.Pixel(910);
                string queries = "";
                switch (filter)
                {
                    case "1":
                        queries = "select * from View_complaint where jenisbuku ='phiwiki 2019'";
                        break;
                    case "2":
                        queries = "select * from View_complaint where jenisbuku ='phiwiki 2020'";
                        break;
                    case "3":
                        queries = "select * from View_complaint where jenisbuku ='phiwiki II 2020'";
                        break;
                    default:
                        queries = "select * from View_complaint";
                        break;
                }
                GetDataComplaint(queries);
                var dataactor = ds.View_complaint.ToList();
                return View(dataactor);
            }
        }
        public void GetDataComplaint(string queries)
        {
            ComplaintDataSet ds = new ComplaintDataSet();
            //Connect to Database
            var connectionString = ConfigurationManager.ConnectionStrings["phiwikiConnectionString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(queries, conn);
            dataAdapter.Fill(ds, ds.View_complaint.TableName);
            report.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Report\Complaint.rdlc";
            report.LocalReport.DataSources.Add(new ReportDataSource("DataSetComplaint", ds.Tables[0]));
            ViewBag.ReportViewer = report;
        }

        public ActionResult Review(string filter)
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id == "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                report.ProcessingMode = ProcessingMode.Local;
                report.Width = Unit.Pixel(910);
                string queries = "";
                switch (filter)
                {
                    case "1":
                        queries = "select * from View_review where jenisbuku ='phiwiki 2019'";
                        break;
                    case "2":
                        queries = "select * from View_review where jenisbuku ='phiwiki 2020'";
                        break;
                    case "3":
                        queries = "select * from View_review where jenisbuku ='phiwiki II 2020'";
                        break;
                    default:
                        queries = "select * from View_review";
                        break;
                }
                GetDataReview(queries);
                var dataactor = ds.View_review.ToList();
                return View(dataactor);
            }
        }
        public void GetDataReview(string queries)
        {
            ReviewDataSet ds = new ReviewDataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["phiwikiConnectionString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(queries, conn);
            //dataAdapter.SelectCommand.Parameters.AddWithValue("@id", 1);
            dataAdapter.Fill(ds, ds.View_review.TableName);
            report.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Report\Review.rdlc";
            report.LocalReport.DataSources.Add(new ReportDataSource("Review", ds.Tables[0]));
            ViewBag.ReportViewer = report;
        }

        public ActionResult Rating(string filter)
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id == "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                report.ProcessingMode = ProcessingMode.Local;
                report.Width = Unit.Pixel(910);
                string queries = "";
                switch (filter)
                {
                    case "1":
                        queries = "select * from View_rating where jenisbuku ='phiwiki 2019'";
                        break;
                    case "2":
                        queries = "select * from View_rating where jenisbuku ='phiwiki 2020'";
                        break;
                    case "3":
                        queries = "select * from View_rating where jenisbuku ='phiwiki II 2020'";
                        break;
                    default:
                        queries = "select * from View_rating";
                        break;
                }
                GetDataRating(queries);
                var dataactor = ds.View_rating.ToList();
                return View(dataactor);
            }
        }
        public void GetDataRating(string queries)
        {
            RatingDataSet ds = new RatingDataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["phiwikiConnectionString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(queries, conn);
            dataAdapter.Fill(ds, ds.View_rating.TableName);
            report.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Report\Rating.rdlc";
            report.LocalReport.DataSources.Add(new ReportDataSource("Rating", ds.Tables[0]));
            ViewBag.ReportViewer = report;
        }

        public ActionResult Quiz(string filter)
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id == "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                report.ProcessingMode = ProcessingMode.Local;
                report.Width = Unit.Pixel(910);
                string queries = "";
                switch (filter)
                {
                    case "1":
                        queries = "select * from View_Quiz order by statusjawaban + 0 DESC";
                        break;
                    case "2":
                        queries = "select * from View_Quiz order by statusjawaban + 0 ASC";
                        break;
                    case "3":
                        queries = "select top 10 * from View_Quiz order by statusjawaban";
                        break;
                    default:
                        queries = "select * from View_Quiz";
                        break;
                }
                GetDataQuiz(queries);
                var dataactor = ds.View_Quiz.ToList();
                return View(dataactor);
            }
        }
        public void GetDataQuiz(string queries)
        {
            QuizDataSet ds = new QuizDataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["phiwikiConnectionString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(queries, conn);
            dataAdapter.Fill(ds, ds.View_Quiz.TableName);
            report.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Report\Quiz.rdlc";
            report.LocalReport.DataSources.Add(new ReportDataSource("Quiz", ds.Tables[0]));
            ViewBag.ReportViewer = report;
        }

        public ActionResult Timeline(string filter)
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id == "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                report.ProcessingMode = ProcessingMode.Local;
                report.Width = Unit.Pixel(910);
                string queries = "";
                switch (filter)
                {
                    case "1":
                        queries = "select * from tbl_timeline where divisi ='superadmin'";
                        break;
                    case "2":
                        queries = "select * from tbl_timeline where divisi ='admin'";
                        break;
                    case "3":
                        queries = "select * from tbl_timeline where divisi ='logistics'";
                        break;
                    case "4":
                        queries = "select * from tbl_timeline where divisi ='problem solver'";
                        break;
                    case "5":
                        queries = "select * from tbl_timeline where divisi ='treasurer'";
                        break;
                    default:
                        queries = "select * from tbl_timeline";
                        break;
                }
                GetDataTimeline(queries);
                var dataactor = ds.tbl_role.ToList();
                return View(dataactor);
            }
        }
        public void GetDataTimeline(string queries)
        {
            TimelineDataSet ds = new TimelineDataSet();
            var connectionString = ConfigurationManager.ConnectionStrings["phiwikiConnectionString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(queries, conn);
            dataAdapter.Fill(ds, ds.tbl_timeline.TableName);
            report.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Report\TimeLine.rdlc";
            report.LocalReport.DataSources.Add(new ReportDataSource("Timeline", ds.Tables[0]));
            ViewBag.ReportViewer = report;
        }

    }
}