﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PHIWIKI.Models;

namespace phiwiki.Controllers.Controller_Admin
{
    public class OrderBookController : Controller
    {
        phiwikiEntities db = new phiwikiEntities();
        // GET: OrderBook
        public ActionResult OrderBook()
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id == "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                return View(db.View_orderbook.ToList());
            }
        }


        public ActionResult Edit(tbl_transaksi_buku g)
        {
            var edit = db.tbl_transaksi_buku.Where(x => x.id_transaksi_buku == g.id_transaksi_buku).SingleOrDefault();
            try
            {
                edit.status = g.status;
                db.SaveChanges();
                TempData["message"] = "Edit Success ! ";
            }
            catch (Exception Error)
            {
                throw Error;
            }
            return RedirectToAction("OrderBook");
        }

        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    db.tbl_transaksi_buku.Remove(db.tbl_transaksi_buku.Find(id));
                    db.SaveChanges();
                    TempData["message"] = "Delete Success ! ";
                }
            }
            catch (Exception error)
            {
                throw error;
            }
            return RedirectToAction("OrderBook");
        }
    }
}