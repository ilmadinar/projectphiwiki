﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PHIWIKI.Models;

namespace phiwiki.Controllers.Controller_Admin
{
    public class MasterTutorialController : Controller
    {
        phiwikiEntities db = new phiwikiEntities();
        // GET: MasterTutorial
        public ActionResult MasterTutorial()
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id == "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                return View(db.tbl_tutorial.ToList());
            }
        }

        public List<tbl_tutorial> Video()
        {
            return db.tbl_tutorial.ToList();

        }
        [HttpPost]
        public ActionResult Create(HttpPostedFileBase VideoFile, string deskripsi)
        {
            tbl_tutorial r = new tbl_tutorial();

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {

                    if (ModelState.IsValid)
                    {

                        r.VideoFile = VideoFile;
                        string fileName = Path.GetFileNameWithoutExtension(r.VideoFile.FileName);
                        string extension = Path.GetExtension(r.VideoFile.FileName);
                        fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                        r.video = "~/Videofiles/" + fileName;
                        fileName = Path.Combine(Server.MapPath("~/Videofiles/"), fileName);
                        r.VideoFile.SaveAs(fileName);

                        r.deskripsi = deskripsi;
                        db.tbl_tutorial.Add(r);
                        db.SaveChanges();
                        TempData["tutorial"] = "Submit successful";
                        TempData.Keep();
                        return RedirectToAction("MasterTutorial");
                    }
                }
                catch (Exception ex)
                {
                    TempData["error"] = "Submit successful";
                    transaction.Rollback();
                    return RedirectToAction("MasterTutorial");
                }
            }
            return RedirectToAction("MasterTutorial");
        }

        public ActionResult Edit(tbl_tutorial g)
        {
            var edit = db.tbl_tutorial.Where(x => x.id_tutorial == g.id_tutorial).SingleOrDefault();
            try
            {
                String fullPath = Request.MapPath(edit.video);
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }

                string fileName = Path.GetFileNameWithoutExtension(edit.VideoFile.FileName);
                string extension = Path.GetExtension(edit.VideoFile.FileName);
                fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                edit.video = "~/Videofiles/" + fileName;
                fileName = Path.Combine(Server.MapPath("~/Videofiles/"), fileName);
                edit.VideoFile.SaveAs(fileName);

                edit.deskripsi = g.deskripsi;
                db.SaveChanges();
                TempData["message"] = "Edit Success ! ";
            }
            catch (Exception Error)
            {
                throw Error;
            }
            return RedirectToAction("MasterQuiz");
        }

        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    db.tbl_tutorial.Remove(db.tbl_tutorial.Find(id));
                    db.SaveChanges();
                    TempData["message"] = "Delete Success ! ";
                }
            }
            catch (Exception error)
            {
                throw error;
            }
            return RedirectToAction("MasterQuiz");
        }

        [HttpGet]
        public ActionResult ViewTutorial(int id)
        {
            tbl_tutorial video = new tbl_tutorial();
            video = db.tbl_tutorial.Where(x => x.id_tutorial == id).FirstOrDefault();
            return View(video);
        }

    }
}
