﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PHIWIKI.Models;

namespace phiwiki.Controllers.Controller_Admin
{
    public class ScheduleController : Controller
    {
        phiwikiEntities db = new phiwikiEntities();
        // GET: Schedule
        public ActionResult Schedule()
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id == "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                return View(db.tbl_jadwal.ToList());
            }
        }

        public ActionResult Create(tbl_jadwal q)
        {
            try
            {
                db.tbl_jadwal.Add(q);
                db.SaveChanges();
                TempData["message"] = "Create Success ! ";
            }
            catch (Exception error)
            {
                throw error;
            }
            return RedirectToAction("Schedule");
        }

        public ActionResult Edit(tbl_jadwal g)
        {
            var edit = db.tbl_jadwal.Where(x => x.id_jadwal == g.id_jadwal).SingleOrDefault();
            try
            {
                edit.tanggal = g.tanggal;
                edit.tempat = g.tempat;
                edit.fakultas= g.fakultas;
                db.SaveChanges();
                TempData["message"] = "Edit Success ! ";
            }
            catch (Exception Error)
            {
                throw Error;
            }
            return RedirectToAction("Schedule");
        }

        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    db.tbl_jadwal.Remove(db.tbl_jadwal.Find(id));
                    db.SaveChanges();
                    TempData["message"] = "Delete Success ! ";
                }
            }
            catch (Exception error)
            {
                throw error;
            }
            return RedirectToAction("Schedule");
        }
    }
}