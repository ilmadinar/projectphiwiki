﻿using Newtonsoft.Json;
using PHIWIKI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;


namespace phiwiki.Controllers.Controller_Admin
{
    public class DashboardController : Controller
    {
        phiwikiEntities db = new phiwikiEntities();
        public ActionResult Dashboard()
        {

            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id == "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {

                List<tbl_transaksi_buku> order = db.tbl_transaksi_buku.ToList();
                List<tbl_user> costumer = db.tbl_user.ToList();
                List<tbl_layanan_pengaduan> complaint = db.tbl_layanan_pengaduan.ToList();
                List<tbl_reimburse> reimburses = db.tbl_reimburse.ToList();
                List<tbl_timeline> timeline = db.tbl_timeline.ToList();
                List<tbl_jawaban_quiz> quiz = db.tbl_jawaban_quiz.ToList();
                List<View_orderbook> myorder = db.View_orderbook.ToList();
                dashboard list = new dashboard();
                list.transaksi = order;
                list.user = costumer;
                list.pengaduan = complaint;
                list.reimburses = reimburses;
                list.timelines = timeline;
                list.quiz = quiz;
                list.myorder = myorder;
                return View(list);
            }


        }

        public JsonResult User()
        {
            var user = db.tbl_user.Where(x => x.fk_id_role == 1).Count();
            return Json(user, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Order()
        {
            var order = db.tbl_transaksi_buku.Select(x => x.id_transaksi_buku).Count();
            return Json(order, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Complaint()
        {
            var complaint = db.tbl_layanan_pengaduan.Select(x => x.id_layanan_pengaduan).Count();
            return Json(complaint, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Amount()
        {
            var amount = db.tbl_transaksi_buku.Select(x => x.harga_total).Sum().ToString("C0", CultureInfo.CreateSpecificCulture("id-ID"));
            return Json(amount, JsonRequestBehavior.AllowGet);
        }

    }
}