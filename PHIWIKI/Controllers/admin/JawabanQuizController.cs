﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using PHIWIKI.Models;

namespace phiwiki.Controllers.Controller_Admin
{
    public class JawabanQuizController : Controller
    {
        phiwikiEntities db = new phiwikiEntities();
        // GET: MasterRole
        public ActionResult JawabanQuiz()
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id == "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                return View(db.tbl_jawaban_quiz.ToList());
            }
        }


        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    db.tbl_jawaban_quiz.Remove(db.tbl_jawaban_quiz.Find(id));
                    db.SaveChanges();
                    TempData["message"] = "Delete Success ! ";
                }
            }
            catch (Exception error)
            {
                throw error;
            }
            return RedirectToAction("JawabanQuiz");
        }


        public ActionResult Email(int? id)
        {
            var edit = db.tbl_user.Where(x => x.id_user == id).SingleOrDefault();
            try
            {
                SendMailQuiz(edit.email);
                db.SaveChanges();
                TempData["message"] = "Email has been sent! ";
            }
            catch (Exception Error)
            {
                throw Error;
            }
            return RedirectToAction("JawabanQuiz");
        }



        [HttpPost]
        public void SendMailQuiz(string email)
        {

            if (ModelState.IsValid)
            {
                var fromAddress = new MailAddress("order.phiwiki@gmail.com", "Phiwiki");
                var toAddress = new MailAddress(email);
                const string fromPassword = "phiwikinawadata";
                const string subject = "Phiwiki - Quiz Winner";
                var body = "Hi, " +
                    "<br/>thank you for participated in the quiz session " +
                    "<br/>" +
                    "<br/>" +
                    "<br/>Congratulations you are the winner of the quiz" +
                    "<br/>" +
                    "<br/> please reply this email for more information about your gift" +
                    "<br/>" +
                    "<br/>" +
                    "<br/>Thankyou--" +
                    "<br/>This email is automatically sent from Phiwiki Web";

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)

                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                })
                {
                    smtp.Send(message);
                }
            }
        }




    }
}