﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using PHIWIKI.Models;

namespace phiwiki.Controllers.Controller_Admin
{
    public class MasterReimburseController : Controller
    {
        phiwikiEntities db = new phiwikiEntities();
        // GET: MasterReimburse
        public ActionResult MasterReimburse()
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id == "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                return View(db.tbl_reimburse);
            }
        }

        public ActionResult ViewReimburse()
        {
            int id_user = Convert.ToInt32(Session["id_user"].ToString());
            return View(db.tbl_reimburse.Where(x => x.fk_id_user == id_user).ToList());
        }

        public ActionResult Edit(tbl_reimburse g)
        {
            var edit = db.tbl_reimburse.Where(x => x.id_reimburse == g.id_reimburse).SingleOrDefault();
            var user = db.tbl_user.Where(x => x.id_user == edit.fk_id_user).SingleOrDefault();
            try
            {
                edit.konfirmasi2 = g.konfirmasi2;
               

                if(g.konfirmasi2 == "accepted")
                {
                    SendMailReimburseAcc(user.email, edit.total_pengeluaran);
                    TempData["message"] = "Edit Success ! ";
                }
                else if (g.konfirmasi2 == "rejected")
                {
                    SendMailReimburseFail(user.email, edit.total_pengeluaran);
                    TempData["message"] = "Edit Success ! ";
                }
                else
                {
                    TempData["message"] = "Edit Success ! ";
                }
                db.SaveChanges();

            }
            catch (Exception Error)
            {
                throw Error;
            }
            return RedirectToAction("MasterReimburse");
        }

        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    var edit = db.tbl_reimburse.Where(x => x.id_reimburse == id).SingleOrDefault();
                    string fullPath = Request.MapPath(edit.konfirmasi1);
                    if (System.IO.File.Exists(fullPath))
                    {
                        System.IO.File.Delete(fullPath);
                    }

                    db.tbl_reimburse.Remove(db.tbl_reimburse.Find(id));
                    db.SaveChanges();
                    TempData["message"] = "Delete Success ! ";
                }
            }
            catch (Exception error)
            {
                throw error;
            }
            return RedirectToAction("MasterReimburse");
        }


        [HttpPost]
        public void SendMailReimburseAcc(string email, int total)
        {
            if (ModelState.IsValid)
            {

                var fromAddress = new MailAddress("order.phiwiki@gmail.com", "Phiwiki");
                var toAddress = new MailAddress(email);
                const string fromPassword = "phiwikinawadata";
                const string subject = "Phiwiki - Reimburse";
                //var body = "Click here to change your password : <a href='https://localhost:44349/Auth/NewPassword/" + objForgot.id_forgot_password + "'>Link</a>";

                var body = "Hi, " +
                    "<br/>Your request for reimbursement has been accepted, " +
                    "<br/>total : Rp." + total +
                    "<br/>" +
                    "<br/>please reply to this email by attaching your bank account number" +
                    "<br/>" +
                    "<br/>--" +
                    "<br/>This email is automatically sent from Phiwiki Web";

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)

                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                })
                {
                    smtp.Send(message);
                }
            }
        }


        [HttpPost]
        public void SendMailReimburseFail(string email, int total)
        {
            if (ModelState.IsValid)
            {

                var fromAddress = new MailAddress("order.phiwiki@gmail.com", "Phiwiki");
                var toAddress = new MailAddress(email);
                const string fromPassword = "phiwikinawadata";
                const string subject = "Phiwiki - Reimburse";
                //var body = "Click here to change your password : <a href='https://localhost:44349/Auth/NewPassword/" + objForgot.id_forgot_password + "'>Link</a>";

                var body = "Hi, " +
                    "<br/>Your request for reimbursement has been rejected, " +
                    "<br/>total : Rp." + total +
                    "<br/>" +
                    "<br/>please request reimbursement again" +
                    "<br/>" +
                    "<br/>--" +
                    "<br/>This email is automatically sent from Phiwiki Web";

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)

                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                })
                {
                    smtp.Send(message);
                }
            }
        }



    }
}