﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PHIWIKI.Models;

namespace phiwiki.Controllers.Controller_Admin
{
    public class MasterCostumerController : Controller
    {
        phiwikiEntities db = new phiwikiEntities();
        // GET: MasterCostumer
        public ActionResult MasterCostumer()
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id == "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                ViewData["role"] = role();
                return View(db.View_role_customer.ToList());
            }
        }

        public List<tbl_role> role()
        {
            return db.tbl_role.ToList();

        }

        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    var edit = db.tbl_keranjangbayar.Where(x => x.fk_id_user == id).ToList();

                    foreach(tbl_keranjangbayar item in edit) {
                        string fullPath = Request.MapPath(item.buktipembayaran);
                        if (System.IO.File.Exists(fullPath))
                        {
                            System.IO.File.Delete(fullPath);
                        }
                    }


                    db.tbl_user.Remove(db.tbl_user.Find(id));
                    db.SaveChanges();
                    TempData["message"] = "Delete Success ! ";
                }
            }
            catch (Exception error)
            {
                throw error;
            }
            return RedirectToAction("MasterCostumer");
        }

        public ActionResult Edit(tbl_user g)
        {
            var edit = db.tbl_user.Where(x => x.id_user == g.id_user).SingleOrDefault();
            try
            {

                edit.id_user = g.id_user;
                edit.kampus = g.kampus;
                edit.nama_user = g.nama_user;
                edit.nim = g.nim;
                edit.no_hp = g.no_hp;
                edit.password_user = g.password_user;
                edit.status = g.status;
                edit.fakultas = g.fakultas;
                edit.confirmpassword = g.password_user;
                edit.fk_id_role = g.fk_id_role;
                edit.code = g.code;
                edit.email = g.email;
                edit.username = g.username;
                db.SaveChanges();
                TempData["message"] = "Edit Success ! ";
            }
            catch (Exception Error)
            {
                throw Error;
            }
            return RedirectToAction("MasterCostumer");
        }




    }
}