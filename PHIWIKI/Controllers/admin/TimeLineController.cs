﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PHIWIKI.Models;

namespace phiwiki.Controllers.Controller_Admin
{
    public class TimeLineController : Controller
    {
        phiwikiEntities db = new phiwikiEntities();
        // GET: Schedule
        public ActionResult TimeLine()
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id == "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                return View(db.tbl_timeline.ToList());
            }
        }
        public ActionResult Create(tbl_timeline q)
        {
            try
            {
                db.tbl_timeline.Add(q);
                db.SaveChanges();
                TempData["message"] = "Create Success ! ";
            }
            catch (Exception error)
            {
                throw error;
            }
            return RedirectToAction("TimeLine");
        }

        public ActionResult Edit(tbl_timeline g)
        {
            var edit = db.tbl_timeline.Where(x => x.id_timeline == g.id_timeline).SingleOrDefault();
            try
            {
                edit.jobdesc = g.jobdesc;
                edit.start = g.start;
                edit.end = g.end;
                edit.divisi = g.divisi;
                edit.progress = g.progress;
                edit.comment = g.comment;
                edit.status = g.status;
                db.SaveChanges();
                TempData["message"] = "Edit Success ! ";
            }
            catch (Exception Error)
            {
                throw Error;
            }
            return RedirectToAction("TimeLine");
        }

        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    db.tbl_timeline.Remove(db.tbl_timeline.Find(id));
                    db.SaveChanges();
                    TempData["message"] = "Delete Success ! ";
                }
            }
            catch (Exception error)
            {
                throw error;
            }
            return RedirectToAction("TimeLine");
        }
    }
}