﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PHIWIKI.Models;

namespace phiwiki.Controllers.Controller_Admin
{
    public class MasterRoleController : Controller
    {
        phiwikiEntities db = new phiwikiEntities();
        // GET: MasterRole
        public ActionResult MasterRole()
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id != "2")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                return View(db.tbl_role.ToList());
            }
        }

        public ActionResult Create(tbl_role q)
        {
            try
            {
                db.tbl_role.Add(q);
                db.SaveChanges();
                TempData["message"] = "Create Success ! ";
            }
            catch (Exception error)
            {
                throw error;
            }
            return RedirectToAction("MasterRole");
        }

        public ActionResult Edit(tbl_role g)
        { 
            var edit = db.tbl_role.Where(x => x.id_role == g.id_role).SingleOrDefault();
            try
            {
                edit.role_name = g.role_name;
                //edit.jawaban_benar = g.jawaban_benar;
                db.SaveChanges();
                TempData["message"] = "Edit Success ! ";
            }
            catch (Exception Error)
            {
                throw Error;
            }
            return RedirectToAction("MasterRole");
        }

        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    db.tbl_role.Remove(db.tbl_role.Find(id));
                    db.SaveChanges();
                    TempData["message"] = "Delete Success ! ";
                }
            }
            catch (Exception error)
            {
                throw error;
            }
            return RedirectToAction("MasterRole");
        }
    }
}