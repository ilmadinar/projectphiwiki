﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PHIWIKI.Models;

namespace phiwiki.Controllers.Controller_Admin
{
    public class PickupDeliveryController : Controller
    {
        phiwikiEntities db = new phiwikiEntities();
        // GET: OrderBook
        public ActionResult PickupDelivery()
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id == "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                return View(db.Jnes.ToList());
            }
        }

        public ActionResult Create(Jne q)
        {
            try
            {
                db.Jnes.Add(q);
                db.SaveChanges();
                TempData["message"] = "Create Success ! ";
            }
            catch (Exception error)
            {
                throw error;
            }
            return RedirectToAction("PickupDelivery");
        }

        public ActionResult Edit(Jne g)
        {
            var edit = db.Jnes.Where(x => x.Id == g.Id).SingleOrDefault();
            try
            {
                edit.origin = g.origin;
                edit.destinasi = g.destinasi;
                edit.destinasi_code = g.destinasi_code;
                edit.tarif_reg = g.tarif_reg;
                edit.est_reg = g.est_reg;
                db.SaveChanges();
                TempData["message"] = "Edit Success ! ";
            }
            catch (Exception Error)
            {
                throw Error;
            }
            return RedirectToAction("PickupDelivery");
        }

        public ActionResult Delete(int? id)
        {
            try
            {
                if (id != null)
                {
                    db.Jnes.Remove(db.Jnes.Find(id));
                    db.SaveChanges();
                    TempData["message"] = "Delete Success ! ";
                }
            }
            catch (Exception error)
            {
                throw error;
            }
            return RedirectToAction("PickupDelivery");
        }
    }
}