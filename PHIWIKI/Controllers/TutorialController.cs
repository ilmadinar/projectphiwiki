﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PHIWIKI.Models;

namespace phiwiki.Controllers
{
    public class TutorialController : Controller
    {
        phiwikiEntities db = new phiwikiEntities();
        // GET: Tutorial
        public ActionResult Tutorial()
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id != "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                return View(db.tbl_tutorial.ToList());
            }
           
        }
    }
}