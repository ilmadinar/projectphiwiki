﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PHIWIKI.Models;

namespace phiwiki.Controllers
{
    public class RatingController : Controller
    {
        phiwikiEntities db = new phiwikiEntities();

        public ActionResult Rating()
        {
            string id = Session["role"].ToString();
            if (Session["username"] == null)
            {
                return RedirectToAction("Login", "User");
            }
            else if (id != "1")
            {
                return RedirectToAction("Permission", "User");
            }
            else
            {
                ViewData["jenisbuku"] = jenisbuku();
                tbl_layanan_rating order = new tbl_layanan_rating();
                return View(order);
            }
            
        }

        public List<tbl_jenisbuku> jenisbuku()
        {
            return db.tbl_jenisbuku.ToList();

        }

        [HttpPost]
        public ActionResult Rating(int rating, string jenisbuku)
        {
            int iduser = Convert.ToInt32(Session["id_user"]);
            var cek = db.view_myorder_transaksi.Where(x => x.fk_id_user == iduser && x.namabuku == jenisbuku && x.status == "done").FirstOrDefault();
            var cekRating = db.tbl_layanan_rating.Where(x => x.fk_id_user == iduser && x.jenisbuku == jenisbuku).FirstOrDefault();
            tbl_layanan_rating r = new tbl_layanan_rating();

            if (cek != null)
            {
                if(cekRating == null)
                {
                    if (ModelState.IsValid)
                    {
                        r.date = DateTime.Now;
                        r.rating_buku = rating;
                        r.jenisbuku = jenisbuku;
                        r.fk_id_user = Convert.ToInt32(Session["id_user"]);
                        db.tbl_layanan_rating.Add(r);
                        db.SaveChanges();
                        TempData["feedback"] = "Your rating has been sent! Thank you";
                        TempData.Keep();
                        return RedirectToAction("Rating");
                    }

                }
                else
                {
                    TempData["feedbackfail"] = "Cannot give rating, you have already rated this book";
                    TempData.Keep();
                    return RedirectToAction("Rating");
                }
                
            }
            else
            {
                TempData["feedbackfail"] = "Cannot give rating, you haven't bought this book yet";
                TempData.Keep();
                return RedirectToAction("Rating");
            }

            return View();
        }


    }
}
