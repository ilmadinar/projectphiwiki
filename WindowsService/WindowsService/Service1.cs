﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Timers;
using System.Web.Hosting;
using System.Web.ModelBinding;
using WindowsService.Models;

namespace WindowsService
{
    [RunInstaller(true)]
    public partial class Service1 : ServiceBase
    {
        System.Timers.Timer tmrExcecutor = new System.Timers.Timer();

        int ScheduleTime = Convert.ToInt32(ConfigurationSettings.AppSettings["ThreadTime"]);
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                tmrExcecutor.Elapsed += new ElapsedEventHandler(tmrExcecutor_Elapsed);
                tmrExcecutor.Interval = 10000;
                tmrExcecutor.Enabled = true;
                tmrExcecutor.Start();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void tmrExcecutor_Elapsed(object sender,System.Timers.ElapsedEventArgs e)
        {
            
            using (phiwikiEntities2 db = new phiwikiEntities2())
            {
                //var datauser = db.tbl_user.Select(x => new { x.email ,x.nama_user}).ToList();
                var datauser = db.tbl_user.Where(x=> x.fk_id_role == 1).ToList();


                    foreach (var item in datauser)
                    {
                        View_Quiz data = db.View_Quiz.Where(x => x.email == item.email).FirstOrDefault();
                        if (data == null)
                        {
                            string name = item.nama_user.ToString();
                            string email = item.email.ToString();
                            WriteToFile("Trying to send email to: " + name + " " + email + " {0}");
                            SendMail(name, email);
                           
                            WriteToFile("Email sent successfully to: " + name + " " + email + " {0}");
                        }
                    }
                    Thread.Sleep(ScheduleTime * 60 * 1000);
                
            }
        }


        protected override void OnStop()
        {
            try
            {
                tmrExcecutor.Enabled = false;
              
            }
            catch (Exception)
            {
                throw;
            }
        }
        private void WriteToFile(string text)
        {
            string path = "C:\\ServiceLog.txt";
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(string.Format(text, DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")));
                writer.Close();
            }
        }


        private void SendMail(string name, string email)
        {

                var fromAddress = new MailAddress("order.phiwiki@gmail.com", "Phiwiki");
                var toAddress = new MailAddress(email);
                const string fromPassword = "phiwikinawadata";
                const string subject = "Phiwiki - Quiz Reminder";

                var body = "Hi, " +
                    
                    "<br/>" +
                    "<br/>" +
                    "<br/>You can join the quiz to get the prizes! the quiz only has 3 days left." +
                     "<br/>visit our web at phiwiki.com" +
                    //"<br/> <a href=" + "" + code + "&forgot=" + forgot + ">Link</a>" +
                    "<br/>" +
                    "<br/>--" +
                    "<br/>This email is automatically sent from Phiwiki Web";

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)

                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                })
                {
                    smtp.Send(message);
                }
            
        }



    }
}
